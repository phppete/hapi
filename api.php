<?php


class hapi {
  public $method;
  public $request;
  public $input;
  public $body;
  public $data;

  public function __construct() {
    //collect pertinent request information
    $this->method = $_SERVER['REQUEST_METHOD'];
    $this->request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
    $this->input = file_get_contents('php://input');

    //assign url input into key/value pairs
    $c = count($this->request);#
    if($c>0) {
      for($i=0;$i<$c; $i+=2) {
        $this->data[$this->request[$i]] = $this->request[$i+1];
      }
    }


    //assign body input into key/value pairs
    $p = parse_str($this->input,$this->body);
    
    //pull the trigger
    $this->handleRequest();
  }

  //Assigns a function for each request method.
  //
  //If you don't care which request method is used
  //then you can squash this down and write a single
  //handler function
  public function handleRequest() {
    //modify this to suit your needs
    switch($this->method) {
      
      case 'GET':
        $this->getHandler();
      break;
      case 'POST':
        $this->softFail("POST Method not allowed on this service");
      break;
      case 'PUT':
        $this->notAllowed();
      break;
      case 'DELETE':
        $this->notAllowed();
      break;
      default:
        $this->notAllowed();
      break;
    }

  }

  //Function to handle GET data
  //
  //You can copy this to make postHandler() etc,
  //should you wish  to handle request methods differently
  public function getHandler() {
    //an example of returning different data based on the request
    $tempdata = [
      'result' => 'OK',
      'Request Made' => $this->data, 
      'Request Body' => $this->input
    ];

    //send appropriate header
    header('Content-Type: application/json');
    //push out the data in the right format
    die(json_encode($tempdata));
  }

  //if you want to block certain HTTP methods
  public function notAllowed() {
    header("HTTP/1.0 405 Method Not Allowed");
    die();
  }

  public function softFail(string $message='There was an error') {
     $tempdata = [
      'result' => 'error',
      'message' => $message
    ];

    //send appropriate header
    header('Content-Type: application/json');
    //push out the data in the right format
    die(json_encode($tempdata));
  }
} 