# README #

A simple barebones REST style API handler which can be easily modified

# USAGE #

Include the class and call the listener by:

$API = new hapi;

You then access it with:

http://www.your-domain.com/path/to/api.php/key/value