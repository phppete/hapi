<?php

/*
 * Example usage
 * 
 * Extend the API and use Faker to generate data where appropriate
 *
 * We'll only allow GET/POST requests
 * We'll gracefully fail a PUT request 
 * And we'll hard fail a DELETE request
*/

//api class
include '../api.php';

//use Faker to get randominformation
require_once 'assets/autoload.php';


//customise for ourselves
class hapiExample extends hapi {


  //override the handler function to suit our implementation
  public function handleRequest() {
    
    switch($this->method) {
      case 'GET':
        $this->getHandler();
      break;
      case 'POST':
        $this->postHandler();
      break;
      case 'PUT':
        $this->softFail("PUT method is not allowed");
      break;
      case 'DELETE':
        $this->notAllowed();
      break;
      default:
        $this->notAllowed();
      break;
    }
  }

  //a custom getHandler() with a data check and generated content
  public function getHandler() {

    //check that the action is 'test' for this example
    if(!isset($this->data['action']) || $this->data['action'] != 'test') {
      $this->softfail("This GET request requires an action of 'test'");
    }

    //good to go, generate our data
    $faker = Faker\Factory::create('en_GB');

    //bundle our return data as we'd like it
    $tempdata = [
        'result' => 'OK',
        'Request' => $this->data, 
        'RequestBody' => $this->input,
        'ReturnData' => $faker->address
      ];

    //send appropriate header
    header('Content-Type: application/json');
    //push out the data in the right format
    die(json_encode($tempdata));
  }

  //a custom postHandler()
  public function postHandler() {

    //check that the action is 'test' for this example
    if(!isset($this->body['action']) || $this->body['action'] != 'test') {
      $this->softfail("This POST request requires an action of 'test'");
    }

    //good to go, generate our data
    $faker = Faker\Factory::create('en_GB');

    $tempdata = [
        'result' => 'OK',
        'Request' => $this->data, 
        'RequestBody' => $this->input,
        'ReturnData' => $faker->name
      ];

    //send appropriate header
    header('Content-Type: application/json');
    //push out the data in the right format
    die(json_encode($tempdata));
  }

  //putHandler() and deleteHandler() don't exist because
  //we fail those in the handleRequest()
  //You can add them here if you wish to use those methods
}

$API = new hapiExample;

var_dump($API);