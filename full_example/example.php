<?php

//example.php

?>

<script
   src="https://code.jquery.com/jquery-3.1.1.min.js"
   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
   crossorigin="anonymous"></script>


<script>
	$(document).ready(function() {

		//GET example
		$.get( "api.php/action/test", function( data ) {
		  $( "#get" ).html( data.ReturnData );
		});

		
		//POST example
		$.post( "api.php", {action: 'test'},function( data ) {
		  $( "#post" ).html( data.ReturnData );
		});

		//PUT
		$.ajax({
		   url: 'api.php',
		   type: 'PUT',
		   success: function( data ) {
		     if(data.result == 'error') {
		     	$("#put").html( data.message );
		     }
		   }
		});

		//DELETE
		$.ajax({
		   url: 'api.php',
		   type: 'DELETE',
		   success: function( data ) {
		     //we know this won't happen in our example
		   },
		    error: function(xhr, textStatus, errorThrown){
		       $("#delete").html( textStatus + " - " + errorThrown );
		    }
		});
	});
</script>

<section>
	<h1>GET Example</h1>
	<p id='get'>
		Waiting ...
	</p>
</section>

<section>
	<h1>POST Example</h1>
	<p id='post'>
		Waiting ...
	</p>
</section>

<section>
	<h1>PUT Example</h1>
	<p id='put'>
		Waiting ...
	</p>
</section>

<section>
	<h1>DELETE Example</h1>
	<p id='delete'>
		Waiting ...
	</p>
</section>